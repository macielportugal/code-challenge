from datetime import datetime

from django.db.models import Count, Sum

from rest_framework.response import Response
from rest_framework import viewsets

from order.models import Order, OrderDetail


class ReportViewSet(viewsets.ViewSet):
    """
    API dos relatórios
    """

    def list(self, request, format=None):
        start = request.GET.get('start')
        end = request.GET.get('end')

        order = OrderDetail.objects.all()

        if not start is None and not end is None:
            start = datetime.strptime(start, '%Y-%m-%d')
            start = start.date()
            end = datetime.strptime(end, '%Y-%m-%d')
            end = end.date()

            order = order.filter(order__created_at__date__range=(start, end))

        sales = order.aggregate(Sum('price'))
        customers = order.aggregate(Count('order__buyer_name'))

        if not sales['price__sum'] is None and not sales['price__sum'] is None:
            ticket = sales['price__sum'] / customers['order__buyer_name__count']
        else:
            ticket = 0
        

        content = {
            'start': start,
            'end': end,
            'sales': sales['price__sum'],
            'customers': customers['order__buyer_name__count'],
            'ticket': ticket
        }

        return Response(content)