from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient, APITestCase
from rest_framework import status


class ReportTestCase(APITestCase):
    fixtures = ['test.json']

    def test_show_report(self):
        api_client = APIClient()
        response = api_client.get(reverse('report'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)