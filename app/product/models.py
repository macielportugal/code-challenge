from django.db import models
from django.utils.translation import gettext as _


class Product(models.Model):
    name = models.CharField(max_length=200, unique=True)
    image = models.ImageField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    stock = models.IntegerField(default=1)
    price = models.DecimalField(decimal_places=2, default=0.0, max_digits=8)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('name',)
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        return self.name


class Attribute(models.Model):
    product = models.ForeignKey(Product, related_name='attributes', on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    value = models.CharField(max_length=200)
