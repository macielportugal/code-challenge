from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS

from product.models import Product, Attribute
from product.serializers import ProductSerializer, AttributeSerializer


class ReadOnly(BasePermission):
    """
    Permitir o acesso sem autenticação apenas para leitura
    """

    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class ProductViewSet(viewsets.ModelViewSet):
    """
    API dos produtos
    """

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticated|ReadOnly,)

    def get_queryset(self):
        products = Product.objects.all()
        return products

class AttributeViewSet(viewsets.ModelViewSet):
    """
    API dos atributos
    """

    queryset = Attribute.objects.all()
    serializer_class = AttributeSerializer
    permission_classes = (IsAuthenticated|ReadOnly,)

    def get_queryset(self):
        attributes = Attribute.objects.all()
        return attributes
