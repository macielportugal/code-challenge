from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

from rest_framework.test import APIClient, APITestCase
from rest_framework.authtoken.models import Token
from rest_framework import status

from product.models import Product


class ProductTestCase(APITestCase):
    fixtures = ['test.json']

    def setUp(self):
        self.product = Product.objects.get(pk=1)
        self.user = User.objects.get(username='test')
        self.token = Token.objects.create(user=self.user)
        self.client_without_token = APIClient()
        self.client_with_token = APIClient()
        self.client_with_token.credentials(
            HTTP_AUTHORIZATION='Token ' + self.token.key
        )
        self.url_list = reverse('product-list')
        self.url_detail = reverse('product-detail', args=(1,))
        self.data = {
            'description': self.product.description,
            'name': self.product.name,
            'price': self.product.price,
            'stock': self.product.stock,
            'attributes': [
                {'name': 'Attr 01', 'value': 'Value 01'},
                {'name': 'Attr 02', 'value': 'Value 02'},
                {'name': 'Attr 03', 'value': 'Value 03'},
            ]
        }

    def test_create_product(self):
        """
        Teste de criação de produto.
        A criação de produto deve ter estas validações.
        1º Não permitir a criação sem token.
        2º Não pode criar se os atributos obrigatório estão em branco.
        3º Não pode cadastrar um produto que já existe.
        """
        
        data = {}
        
        # 1º Teste
        response = self.client_without_token.post(self.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # 2º Teste
        response = self.client_with_token.post(self.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, 'This field is required.', status_code=status.HTTP_400_BAD_REQUEST)

        # 3º Teste
        data = self.data
                
        response = self.client_with_token.post(self.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, 'Product with this name already exists.', status_code=status.HTTP_400_BAD_REQUEST)

        data['name'] = 'Product 03'
        response = self.client_with_token.post(self.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_show_product(self):
        """
        Teste de listagem de produtos.
        """

        response = self.client_without_token.get(self.url_list, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client_with_token.get(self.url_list, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Product 01', status_code=status.HTTP_200_OK)
    
        response = self.client_with_token.get(self.url_detail, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Product 01', status_code=status.HTTP_200_OK)

    def test_update_product(self):
        """
        Teste de atualização de produto.
        Regra de validação.
        1º Não permitir a atualizar sem token.
        2º Não pode atualizar se os atributos obrigatório estão em branco.
        3º Não pode atualizar para um produto com o nome já exista.
        """

        # 1º Teste
        data = self.data
        response = self.client_without_token.put(self.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        
        # 2º Teste
        data['name'] = ''
        response = self.client_with_token.put(self.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, 'This field may not be blank.', status_code=status.HTTP_400_BAD_REQUEST)
        
        # 3º Teste
        data['name'] = 'Product 02'
        response = self.client_with_token.put(self.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, 'Product with this name already exists.', status_code=status.HTTP_400_BAD_REQUEST)


        data['name'] = 'New name'
        response = self.client_with_token.put(self.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_product(self):
        """
        Teste de remoção de produto.
        Regra de validação.
        1º Não permitir remove sem token
        """
    
        # 1º Teste
        response = self.client_without_token.delete(self.url_detail)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        
        response = self.client_with_token.delete(self.url_detail)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

