from rest_framework import serializers
from product.models import Product, Attribute


class AttributeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attribute
        fields = ('name', 'value')


class ProductSerializer(serializers.ModelSerializer):
    attributes = AttributeSerializer(many=True)

    class Meta:
        model = Product
        fields = '__all__'

    def create(self, validated_data):
        attributes = validated_data.pop('attributes')
        product = Product.objects.create(**validated_data)
        for attribute in attributes:
            Attribute.objects.create(product=product, **attribute)
        return product

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.image = validated_data.get('image', instance.image)
        instance.description = validated_data.get('description', instance.description)
        instance.stock = validated_data.get('stock', instance.stock)
        instance.price = validated_data.get('price', instance.price)
        instance.created_at = validated_data.get('created_at', instance.created_at)
        instance.updated_at = validated_data.get('updated_at', instance.updated_at)
        
        attributes = validated_data.pop('attributes')    
        for attribute in attributes:
            Attribute.objects.create(product=instance, **attribute)
        return instance


