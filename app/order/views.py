from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS

from product.views import ReadOnly
from order.models import Order, OrderDetail
from order.serializers import OrderSerializer, OrderDetailSerializer


class OrderViewSet(viewsets.ModelViewSet):
    """
    API dos pedidos
    """

    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (IsAuthenticated|ReadOnly,)


class OrderDetailViewSet(viewsets.ModelViewSet):
    """
    API dos detalhes do pedido
    """

    queryset = OrderDetail.objects.all()
    serializer_class = OrderDetailSerializer
    permission_classes = (IsAuthenticated|ReadOnly,)
