import random

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone

from product.models import Product


class Order(models.Model):
    STATUS_NEW = 'new'
    STATUS_APPROVED = 'approved'
    STATUS_DELIVERED = 'delivered'
    STATUS_CANCELED = 'canceled'

    STATUS_CHOICES = (
        (STATUS_NEW, _('New')),
        (STATUS_APPROVED, _('Approved')),
        (STATUS_DELIVERED, _('Delivered')),
        (STATUS_CANCELED, _('Canceled')),
    )

    number = models.BigIntegerField(_('Number'))
    buyer_name = models.CharField(_('Buyer Name'), max_length=200)
    status = models.CharField(_('Status'), max_length=3, choices=STATUS_CHOICES, default=STATUS_NEW)
    freight = models.DecimalField(decimal_places=2, default=0.0, max_digits=8)
    created_at = models.DateTimeField(_('Created At'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Updated At'), auto_now=True)

    class Meta:
        ordering = ('buyer_name',)
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        return self.buyer_name


class OrderDetail(models.Model):
    order = models.ForeignKey(Order, verbose_name=_('Order'), related_name='items', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, verbose_name=_('Product'), on_delete=models.CASCADE)
    amount = models.IntegerField(_('Amount'))
    price = models.DecimalField(decimal_places=2, default=0.0, max_digits=8)

    class Meta:
        verbose_name = _('Order Detail')
        verbose_name_plural = _('Order Details')

    def __str__(self):
        return self.order.buyer_name


@receiver(pre_save, sender=Order)
def save_order(sender, instance, *args, **kwargs):
    if instance.number is None:
        instance.number = '{:%Y%m%d%H%M%S}{}'.format(timezone.now(), random.getrandbits(16))

@receiver(pre_save, sender=OrderDetail)
def save_order_detail(sender, instance, *args, **kwargs):
    if instance.pk is None:
        instance.product.stock = instance.product.stock - instance.amount
        instance.product.save()