from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

from rest_framework.test import APIClient, APITestCase
from rest_framework.authtoken.models import Token
from rest_framework import status

from order.models import Order
from product.models import Product


class OrderTestCase(APITestCase):
    fixtures = ['test.json']

    def setUp(self):
        self.order = Order.objects.get(pk=2)
        self.product = Product.objects.get(pk=1)
        self.user = User.objects.get(username='test')
        self.token = Token.objects.create(user=self.user)
        self.client_without_token = APIClient()
        self.client_with_token = APIClient()
        self.client_with_token.credentials(
            HTTP_AUTHORIZATION='Token ' + self.token.key
        )
        self.url_list = reverse('order-list')
        self.url_detail = reverse('order-detail', args=(2,))

        items = []

        for item in self.order.items.all():
            items.append({
                'product': item.product.pk,
                'amount': 5,
                'price': item.price
            })

        self.data = {
            'buyer_name': self.order.buyer_name,
            'status': self.order.status,
            'freight': self.order.freight,
            'items': items
        }

    def test_create_order(self):
        """
        Teste de criação de order.
        A criação de order deve ter estas validações.
        1º Não permitir a criação sem token.
        2º Não pode criar se os atributos obrigatório estão em branco.
        3º Não pode cadastrar uma order com produto sem estoque.
        4º Não pode comprar uma quantidade maior de produtos que do estoque
        5º Ao criar uma ordem a quantidade de estoque do produto deve diminui
        """

        data = {}

        # 1º Teste
        response = self.client_without_token.post(self.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # 2º Teste
        response = self.client_with_token.post(self.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, 'This field is required.', status_code=status.HTTP_400_BAD_REQUEST)

        # 3º Teste
        data = self.data
        self.product.stock = 0
        self.product.save()
        response = self.client_with_token.post(self.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, 'product without stock', status_code=status.HTTP_400_BAD_REQUEST)

        # 4º Teste
        self.product.stock = 4
        self.product.save()
        response = self.client_with_token.post(self.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, 'quantity of products is greater than in stock', status_code=status.HTTP_400_BAD_REQUEST)

        # 5º Teste
        self.product.stock = 400
        self.product.save()

        self.assertEqual(Product.objects.get(pk=1).stock, 400)
        response = self.client_with_token.post(self.url_list, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Product.objects.get(pk=1).stock, 395)

    def test_show_order(self):
        """
        Teste de listagem de ordens.
        """

        response = self.client_without_token.get(self.url_list, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client_with_token.get(self.url_list, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Customer 01', status_code=status.HTTP_200_OK)
    
        response = self.client_with_token.get(self.url_detail, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Customer 01', status_code=status.HTTP_200_OK)

    def test_update_order(self):
        """
        Teste de atualização de order.
        A atualização de order deve ter estas validações.
        1º Não permitir a atualização sem token.
        2º Não pode atualizar se os atributos obrigatório estão em branco.
        """

        data = {}

        # 1º Teste
        response = self.client_without_token.put(self.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # 2º Teste
        response = self.client_with_token.put(self.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertContains(response, 'This field is required.', status_code=status.HTTP_400_BAD_REQUEST)

        data = self.data
        response = self.client_with_token.put(self.url_detail, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_order(self):
        """
        Teste de remoção de order.
        Regra de validação.
        1º Não permitir remove sem token
        """
    
        # 1º Teste
        response = self.client_without_token.delete(self.url_detail)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        
        response = self.client_with_token.delete(self.url_detail)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

