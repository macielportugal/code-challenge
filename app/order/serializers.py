from rest_framework import serializers
from product.models import Product
from order.models import Order, OrderDetail


class OrderDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetail
        fields = ('id', 'amount', 'price', 'product')


class OrderSerializer(serializers.ModelSerializer):
    items = OrderDetailSerializer(many=True)

    class Meta:
        model = Order
        fields = ('buyer_name', 'status', 'freight', 'items',)
        read_only = ('number', 'created_at', 'updated_at',)

    def create(self, validated_data):
        items = validated_data.pop('items')

        for item in items:
            if item['product']:
                if item['product'].stock <= 0:
                    raise serializers.ValidationError('product without stock')

                if (item['product'].stock - item['amount']) < 0:
                    raise serializers.ValidationError('quantity of products is greater than in stock')
            else:
                raise serializers.ValidationError('empty product')

        order = Order.objects.create(**validated_data)
        
        for item in items:
            OrderDetail.objects.create(order=order, **item)
        
        return order

    def update(self, instance, validated_data):
        instance.buyer_name = validated_data.get('buyer_name', instance.buyer_name)
        instance.status = validated_data.get('status', instance.status)
        instance.freight = validated_data.get('freight', instance.freight)
        
        items = validated_data.pop('items')    
        for item in items:
            OrderDetail.objects.create(order=instance, **item)
        return instance


