# Code Challenge

## Resumo

A api pode se usada no terminal ou na web pela interface.

## Requisitos
* <a href="https://www.docker.com/">Docker</a>
* <a href="https://docs.docker.com/compose/">Docker Compose</a>

## Instalação
```bash
git clone git@bitbucket.org:macielportugal/code-challenge.git
cd code-challenge
docker-compose build
docker-compose up -d
```

## Uso
O sistema pode se gerenciado pela interface web na url <a href="http://127.0.0.1:8000/admin">http://127.0.0.1:8000/admin</a> ou por <a href="http://127.0.0.1:8000/api">API</a>.

Em ambos os casos é necessário criar um usuário e um token de acesso, os mesmos
podem se criados com os comandos abaixo.

```bash
docker-compose exec web python manage.py migrate
docker-compose exec web python manage.py createsuperuser --username admin --email admin@admin.com
docker-compose exec web python manage.py drf_create_token admin
```
<br />

### API

#### Produtos

Listagem de produtos:

```bash
curl -X GET http://127.0.0.1:8000/api/products/
```

Criação de produto:

```bash
curl -X POST  http://127.0.0.1:8000/api/products/  -H "Authorization: Token cff0071b78232725662b7726c5de2809c90749d5"  -H "Content-Type:application/json"  -d '{"description": "Produto Desc", "name": "Produto", "price": 1.99, "stock": 2, "attributes": [{"name": "Cor", "value": "Verde", "name": "Tamanho", "value": "5cm"}]}'
```

Visualização do produto:

```bash
curl -X GET http://127.0.0.1:8000/api/products/3/
```

Atualização de produto:

```bash
curl -X PUT  http://127.0.0.1:8000/api/products/3/  -H "Authorization: Token bafbbfc1752f70d8b15e23b6a8deae20ee17ef1b"  -H "Content-Type:application/json"  -d '{"description": "Produto Desc", "name": "Produto", "price": 1.99, "stock": 2, "attributes": [{"name": "Cor", "value": "Verde", "name": "Tamanho", "value": "5cm"}]}'
```

Remoção de produto:

```bash
curl -X DELETE  http://127.0.0.1:8000/api/products/2/  -H "Authorization: Token cff0071b78232725662b7726c5de2809c90749d5"
```

#### Pedidos

Listagem de pedidos:

```bash
curl -X GET http://127.0.0.1:8000/api/orders/
```

Criação de pedido:

```bash
curl -X POST  http://127.0.0.1:8000/api/orders/  -H "Authorization: Token bafbbfc1752f70d8b15e23b6a8deae20ee17ef1b"  -H "Content-Type:application/json"  -d '{"buyer_name": "Fulano", "status": "new", "freight": "12.5", "items": [{"product": 1, "amount": "1", "price": "12.2"}]}'
```

Visualização do pedido:

```bash
curl -X GET http://127.0.0.1:8000/api/orders/2/
```

Atualização de pedido:

```bash
curl -X PUT  http://127.0.0.1:8000/api/orders/2/  -H "Authorization: Token bafbbfc1752f70d8b15e23b6a8deae20ee17ef1b"  -H "Content-Type:application/json"  -d '{"buyer_name": "Fulano", "status": "new", "freight": "12.5", "items": [{"product": 1, "amount": "1", "price": "12.2"}]}'
```

Remoção da pedido:

```bash
curl -X DELETE  http://127.0.0.1:8000/api/orders/2/  -H "Authorization: Token cff0071b78232725662b7726c5de2809c90749d5"
```

#### Relatório

```bash
curl -X GET http://127.0.0.1:8000/api/report/
```

*Filtros:*
start = Data de inicio
end = Data de fim

```bash
curl -X GET http://127.0.0.1:8000/api/report/?start=2019-04-13&2019-04-13
```

## Testes

Os testes cobrem todas as etapas da API.

Arquivo com os testes app/product/tests.py
Arquivo com os testes app/order/tests.py
Arquivo com os testes app/report/tests.py

```bash
docker-compose exec web python manage.py test
// ou
docker-compose exec web python manage.py test order
```
